# LOGBOOK WEEK 3

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 4

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

The goal for this week is to finish the CATIA drawing of the airship, in order to simulate the airflow and get the parameters needed through Ansys

### **What decisions did you/your team make in solving a problem?**

As we have already divided ourselves with the specific task, the CATIA team proceed with the drawing right after they measured and confirmed the dimension of the real airship


### **How did you/your team make those decisions (method)?**

The specific task assigned is to ensure everyone has their own responsibility in the group. However, each of us should contribute and help each other if there are any problems while completing the task. The CATIA team has three airship design geometry and we select only one of it with the least error.


### **Why did you/your team make that choice (justification) when solving the problem?**
	
The decision were made in order to ensure the validity of the result that we get after simulating it in the Ansys

### **What was the impact on you/your team/the project when you make that decision?**

This may increase the accuracy of the result as the airship design geometry would give an impact in the 'mesh' setup from Ansys


### **What is the next step?**
	
The next step is to simulate the airship in the Ansys software and get the parameters needed such as the Cl, Cd and Cp


