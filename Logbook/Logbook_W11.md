# LOGBOOK WEEK 11

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 11

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

The agenda for this week is to finish the calculation of aerodynamic vector of the airship

### **What decisions did you/your team make in solving a problem?**
We start to check other parameters needed in the equation of aerodynamic vector after getting the result of Cm through Ansys. So, we found out that parameters such as longitudinal, lateral and normal vector needs a further discussion with the other members

### **How did you/your team make those decisions (method)?**

To ensure the precision of our output results in excel


### **Why did you/your team make that choice (justification) when solving the problem?**

We managed to meet up and clear up all the uncertainties of the parameters in the equation with the team members and Dr.


### **What was the impact on you/your team/the project when you make that decision?**

The calculation for the aerodynamic vector is completed after the meeting and we clear up the misunderstanding on meaning of certain values in the equation


### **What is the next step?**
	
The results of the aerodynamic vector is presented and we are required to find the value of crossflow drag coefficient from the equation
