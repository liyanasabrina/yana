# LOGBOOK WEEK 7

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 7

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

The agenda for this week is to validate the result based on the setup that we did on Ansys

### **What decisions did you/your team make in solving a problem?**

We try to find the Cl, Cd and Cm based on the model on the book, however the result is not similar such that we get the negative number for Cl and Cd. This is may be due to low meshing quality and setup problem in Ansys


### **How did you/your team make those decisions (method)?**

So, we try to increase the meshing quality by increasing the number of element and do inflation method in meshing part, as well as checking the setup that we made


### **Why did you/your team make that choice (justification) when solving the problem?**

Increasing the meshing quality could help in terms of accuracy in the result. 

### **What was the impact on you/your team/the project when you make that decision?**

The result starts to show a positive of Cl and Cd, however the value is still not similar with the data from the book


### **What is the next step?**
	
We should check the geometry and all other parameters used in the book before simulating it on Ansys. All the parameters should be the same if we want to make the comparison in terms of validation of the result

