# LOGBOOK WEEK 9

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 9

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

As the setup of our Ansys is correct, we start to simulate and find the aerodynamic parameter of the real airship using Ansys. The real airship results of Cl and Cd is similar when compared with the senior's airship thesis. So, we can start to find the aerodynamic vector properties for the real airship by using formulas and results from Ansys

### **What decisions did you/your team make in solving a problem?**
 We discuss on the parameters needed from the formulas before calculating each of the values. So, in finding the aerodynamic vector of the real airship, we are lacking the Cm values due to some uncertainty in producing the results of Cm in Ansys. So we try to refer to other research paper on CFD analysis of coefficent of moment based on their method

### **How did you/your team make those decisions (method)?**

However, we try to find other way on the Cm results in Ansys by asking seniors who are experts in CFD on how to get the value of Cm accurately through Ansys


### **Why did you/your team make that choice (justification) when solving the problem?**

There are some senior who made their research on the coefficient of moment produced of the moving body in fluid through Ansys. So, we can refered with them based on their experience and knowledge in using Ansys


### **What was the impact on you/your team/the project when you make that decision?**

We can directly ask seniors anything if we had any problem while getting the result, so it will cut the time on getting the accurate results of Cm


### **What is the next step?**
	
We can start to calculate all the paramenter by inserting the formulas in excel

