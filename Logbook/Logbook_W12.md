# LOGBOOK WEEK 12

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 12

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

The agenda for this week is to finish the calculation of aerodynamic vector of the airship

### **What decisions did you/your team make in solving a problem?**
One of our teammates meet up with the Dr at the lab for the confirmation on the calculation of crossflow drag

### **What was the impact on you/your team/the project when you make that decision?**

The calculation for the aerodynamic vector is completed


### **What is the next step?**
	
We can proceed with the written report of our subsystem
