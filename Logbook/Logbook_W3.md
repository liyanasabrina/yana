# LOGBOOK WEEK 3

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 3

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

Our goals for this week is to take the measurement of the airship and start to design it in the CATIA software in order to simulate the airflow as well as getting the lift and drag force

### **What decisions did you/your team make in solving a problem?**

We discussed with our group mates who have the free time to come to the lab and get the real airship's geometry. The dimensions of design measurement of the physical airship were taken from the top view, side view and bottom view.


### **How did you/your team make those decisions (method)?**

The decision on our team members who will come to the lab was made based on their availability and convenience, so a few of our group mates who stay at the college and near the campus will come to the lab. In terms of measurement, tolerance is considered after taking the measurement as there might be some error while measuring the exact dimension of the airship


### **Why did you/your team make that choice (justification) when solving the problem?**
	
Error could occur whenever experiment is conducted, so the tolerance should be included for the purpose of making the dimension is within the range of the design value

### **What was the impact on you/your team/the project when you make that decision?**

The tolerance is made to ensure a good illustration of the design outcome can be produced. Thus, the calculation parameter involved such as lift and drag is reasonable and can be accepted


### **What is the next step?**
	
The next step is to start the designing process in CATIA. The design of the airship for the first flight test may only involved the airship's body, excluding the thrusters, gondola and sprayer


