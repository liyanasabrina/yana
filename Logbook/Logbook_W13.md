# LOGBOOK WEEK 13

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 13

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

Finish the simulation subsystem report

### **What decisions did you/your team make in solving a problem?**
We discussed on the contents and assigned each of us with the subtopics of the report


### **What was the impact on you/your team/the project when you make that decision?**

We managed to finish the report

### **What is the next step?**
	
Help the other subsystem for the pre-flight test of the airship
