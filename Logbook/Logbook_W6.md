# LOGBOOK WEEK 6

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 6

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

This week agenda is to plot and compute the value needed by other subsystem such as Cl, Cd, Cd0 and Cm through Ansys software

### **What decisions did you/your team make in solving a problem?**

Everyone in the group collaborates together on simulating and familiarizing with the Ansys setup. This is to ensure that any problem regarding the Ansys software can be discussed together to make a better judgement and decision. Not only that, we also decided to meet up at the lab to discuss and compute the result together


### **How did you/your team make those decisions (method)?**

It is more convenience for us to meet up and simulate the result together, so that any decision or problem regarding the software can be discussed on the spot with the helps from Dr and seniors in the lab

### **Why did you/your team make that choice (justification) when solving the problem?**

The decision to go to the lab is made due to uncertainty in some parameters and setup in the software. Therefore, based on the understanding from the Airship and Design book, as well as with the helps of seniors, we can asked and verify the validity of our job and results

### **What was the impact on you/your team/the project when you make that decision?**

The guide from the Dr and seniors is to ensure that our team is on the right track. Besides, more data needs to be analyse and discussed with the Dr before we compute it 


### **What is the next step?**
	
The next goal is to validate our result by comparing it with the theoretical value based on the Airship and Design book

