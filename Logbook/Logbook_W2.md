# LOGBOOK WEEK 2

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 2 

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**
We need to discuss and planned about our milestone for the next 14 weeks regarding the project specifically in a simulation team

### **What decisions did you/your team make in solving a problem?**
	
We decided to discuss our plan first after receiving briefing from the project’s manager on the task that we need to complete before the flight-testing process, and proceed with the Gantt chart

### **How did you/your team make those decisions (method)?**
	
Based on the briefing given, we agreed to divide ourselves into smaller groups with the specific task (sub-groups). For instance, the sub-groups consist of the team for the CATIA drawing of the airship, flight performance calculation and air simulation. However, each of the members should cooperate in helping each other in completing every task


### **Why did you/your team make that choice (justification) when solving the problem?**
	
This is to ensure every member took part in finishing this project and all of us should contributes and participates in assisting each other with all the task 

### **What was the impact on you/your team/the project when you make that decision?**
	
Everyone have their own responsibility in completing their task, however, not every member will gain more experience and knowledge in particular task that are not appointed for them

### **What is the next step?**
	
Start to learn and prepare ourselves with the knowledge needed to complete the task. For example, the air simulation sub-groups should be prepared with the CFD knowledge on the computational software and the aerodynamics theory


