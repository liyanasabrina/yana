# LOGBOOK WEEK 5

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 5

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

The goal for this week is to familiarize with the Ansys and try to find the experimental coefficient parameters needed through the software

### **What decisions did you/your team make in solving a problem?**

We read the book related to airship design as well as asking the seniors on the validity of videos that we refer to in using Ansys for the project. The Ansys team try to simulate the flow but a few of them had a problem with the laptop crash while using the Ansys


### **How did you/your team make those decisions (method)?**

More people is needed to try out the simulation tasks on Ansys in order to have the backup data whenever we encounter any problems such as 'crash' on their laptop when using Ansys, lower mesh quality and setup error


### **Why did you/your team make that choice (justification) when solving the problem?**

This is to ensure the data is not missing while using the Ansys and more will get the knowledge on how to use Ansys

### **What was the impact on you/your team/the project when you make that decision?**

The missing data problem can be avoided as we have backup data from other members. Not only that, we can discussed and try to help each other out on the setup problems together with the team


### **What is the next step?**
	
The next goal is to generate the result report on each experimental parameter needed by other subsystem

