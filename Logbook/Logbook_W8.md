# LOGBOOK WEEK 8

### **Name**: Nur Liyana Sabrina Binti Muhd Arif
### **No matric**: 197235
### **Group**: Simulation
### **Week**: 8

<p>&nbsp;</p>

### **What is the agenda this week and what are my/our goals?**

The agenda for this week is to validate our setup by comparing the result from Ansys with the experimental results of research work

### **What decisions did you/your team make in solving a problem?**

We validate our setup process using NACA 0012 and compare it with the experimental results of research paper of NACA 0012


### **How did you/your team make those decisions (method)?**

There are many experimental and CFD works analysis on aerodynamic parameters on NACA 0012 has been done, so we have many paper that we can refered to if we had any problem while simulating the airfoil on Ansys


### **Why did you/your team make that choice (justification) when solving the problem?**

By using specific airfoil with the same characteristics and parameters as in the research paper, and  by comparing both results will determined the validity of our setup

### **What was the impact on you/your team/the project when you make that decision?**

The results from Ansys is similar with the experimental result in the research paper. Both value and graph is compared and it depicts that our setup is correct


### **What is the next step?**
	
The next step is we can start to simulate the real airship by using the same setup in  getting the value of Cl, Cd and Cm

