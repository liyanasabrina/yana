#### Introduction

<img src = "/uploads/ba7b3765a48c1da96387aa85d984201e/hmm.jpeg" width="150" height="150" />

##### Name: Nur Liyana Sabrina
##### From: Selangor
##### Strength & Weakness
| Strength | Weakness |
|----------|----------|
| Positive | Overthinking |
| Organized | Sensitive |
